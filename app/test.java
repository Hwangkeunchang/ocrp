/**
 * Created by 서울어코드 on 2016-10-14.
 */
import java.io.*;
import java.util.*;
import com.amazonaws.*;
import com.amazonaws.services.s3.*;
import com.amazonaws.services.s3.model.*;
import com.amazonaws.services.s3.transfer.*;
import com.amazonaws.auth.*;
public class test {
    public static void main(String[] args) throws IOException {
        /* ---------------------------------------------------------
         * You need to set 'Proxy host', 'Proxy port' and 'Protocol'
         * --------------------------------------------------------- */
        ClientConfiguration config = new ClientConfiguration();
        config.setProxyHost("121.134.178.250"); // LeoFS Gateway's Host
        config.setProxyPort(8080);        // LeoFS Gateway's Port
        config.withProtocol(Protocol.HTTP);
        config.setSignerOverride("S3SignerType");

        final String accessKeyId = "7a4a23a0af0d5347c4ee";
        final String secretAccessKey = "9880fea17adf4b64007606ec052353ffaf5021cb";

        AWSCredentials credentials = new BasicAWSCredentials(accessKeyId, secretAccessKey);
        AmazonS3 s3 = new AmazonS3Client(credentials, config);

        final String bucketName = "wansu" + UUID.randomUUID();
        final String key = "samplekey";

        try {
            // Create a bucket
            s3.createBucket(bucketName);

            // Retrieve list of buckets
            for (Bucket bucket : s3.listBuckets()) {
                System.out.println("Bucket:" + bucket.getName());
            }

            // PUT an object into the LeoFS
            s3.putObject(new PutObjectRequest(bucketName, key, createFile()));

            // GET an object from the LeoFS
            S3Object object = s3.getObject(new GetObjectRequest(bucketName, key));
            dumpInputStream(object.getObjectContent());

            // Retrieve list of objects from the LeoFS
            ObjectListing objectListing =
                    s3.listObjects(new ListObjectsRequest().withBucketName(bucketName));

            for (S3ObjectSummary objectSummary : objectListing.getObjectSummaries()) {
                System.out.println(objectSummary.getKey() +
                        "Size:" + objectSummary.getSize());
            }

            // DELETE an object from the LeoFS
            //s3.deleteObject(bucketName, key);

            // DELETE a bucket from the LeoFS
            //s3.deleteBucket(bucketName);

        } catch (AmazonServiceException ase) {
            System.out.println(ase.getMessage());
            System.out.println(ase.getStatusCode());
        } catch (AmazonClientException ace) {
            System.out.println(ace.getMessage());
        }
    }

    private static File createFile() throws IOException {
        File file = File.createTempFile("leofs_test", ".txt");

        file.deleteOnExit();

        Writer writer = new OutputStreamWriter(new FileOutputStream(file));
        writer.write("Hello, world!\n");
        writer.close();

        return file;
    }

    private static void dumpInputStream(InputStream input) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(input));
        while (true) {
            String line = reader.readLine();
            if (line == null) break;
            System.out.println(line);
        }
    }

}