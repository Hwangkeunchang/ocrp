package models

/**
  * Created by ?쒖슱?댁퐫??on 2016-11-13.
  */
import java.nio.ByteBuffer
import java.util.Date
import java.util.Locale.Category
import java.util.UUID

import com.websudos.phantom.dsl
import com.websudos.phantom.dsl._
import play.api.libs.json.{JsPath, Json, Writes}

import scala.concurrent.Future
import scala.util.Random


case class Board(uuid:UUID, no:Int, title :String, content : String, owner : UUID, names:String, date:DateTime, viewss:Int, category:Int)

abstract class Boards extends CassandraTable[ConcreteBoards, Board]{
  object uuid      extends UUIDColumn(this) with PartitionKey[UUID]
  object no        extends IntColumn(this) with Index[Int]
  object title     extends StringColumn(this) with Index[String]
  object content   extends StringColumn(this) with Index[String]
  object owner     extends UUIDColumn(this) with Index[UUID]
  object names      extends StringColumn(this)
  object date      extends DateTimeColumn(this) with Index[DateTime]
  object viewss      extends IntColumn(this)
  object category  extends IntColumn(this) with Index[Int]

  def fromRow(row: Row): Board = {
    Board(
      uuid = uuid(row),
      no = no(row),
      title = title(row),
      content = content(row),
      owner = owner(row),
      names = names(row),
      date = date(row),
      viewss = viewss(row),
      category = category(row)
    )
  }
}

abstract class ConcreteBoards extends Boards with RootConnector{
  def getAll() : Future[List[Board]]={
    select.fetch()
  }

  def search_board(uuid:UUID) : Future[Option[(String, String, String, DateTime,Int,Int)]]={
    select(_.title, _.content, _.names, _.date, _.viewss, _.category).where(_.uuid eqs uuid).one
  }

  def getBoard() : Future[List[(UUID, Int, String, String, DateTime, Int)]] ={
    select(_.uuid, _.no, _.title, _.names, _.date, _.viewss).where(_.category eqs 2).fetch()
  }

  def getcount(category: Int) : Future[Option[Long]] ={
    select.count.where(_.category eqs category).one()
  }

  def getnotice() : Future[List[(UUID, String, Int, String, DateTime)]] = {
    select(_.uuid, _.names, _.category, _.title, _.date).where(_.category < 2).allowFiltering().fetch()
  }

  def ModiyBoard(uuid:UUID, title:String, content : String, Category:Int):Future[ResultSet] ={
    update.where(_.uuid eqs uuid).modify(_.title setTo title).and(_.category setTo Category).and(_.content setTo content).future()
  }

  def DeleteBoard(uuid : UUID):Future[ResultSet]={
    delete.where(_.uuid eqs uuid).future()
  }

  def setBoard(title :String, no:Int, content : String, owner: UUID, names : String, date:DateTime, categorys:Int): Future[ResultSet] ={
    println("insert uuid : "+ title + ", content : " + content + ", name : " + names)
    insert.value(_.uuid , UUID.randomUUID()).value(_.no, no).value(_.title , title).value(_.content, content).value(_.owner, owner).value(_.names, names).value(_.date, date).value(_.viewss, 0).value(_.category, categorys).future()
  }

  def creat(): Future[ResultSet] = {
    create
      .ifNotExists
      .future()
  }
}

object Board {

  implicit val BoardReads = Json.reads[Board] //create json fromat

  implicit val BoardWrites = Json.writes[Board]

  implicit val BoardFormat = Json.format[Board]
}

case class noticeDetailData(title:String, content:String, names:String, date:DateTime, viewss:Int, category: Int)

object noticeDetailData{
  implicit val noticeDetailDataFormat = Json.format[noticeDetailData]
}

case class noticeData(uuid:UUID, name:String, category:Int, title:String, date:DateTime)

object noticeData{
  implicit val noticeDataFormat = Json.format[noticeData]
}

case class boardData(uuid:UUID, no:Int, title:String, names:String, date:DateTime, viewss:Int)

object boardData{
  implicit val boardDataFormat = Json.format[boardData]
}