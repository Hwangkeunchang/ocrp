package models


import java.nio.ByteBuffer
import java.util.Date
import java.util.UUID

import com.datastax.driver.core.PagingState
import com.websudos.phantom.dsl
import com.websudos.phantom.dsl._
import play.api.libs.json.{JsPath, Json, Writes}

import scala.concurrent.Future

/**
  * Created by root on 16. 10. 26.
  */
case class MetaData(
                     uuid:UUID,           //Leofs fileName
                     name:String,         //Cassandra & DataTable filename
                     category:Int,
                     ocr:String,          //scan Tesseract-ocr
                     ocr_result:String,   //Show Tesseract-ocr
                     date:DateTime,       // Sorting
                     training_v:Option[UUID],
                     owner:UUID
                   )
abstract class MetaDatas extends CassandraTable[ConcreteMetaDatas, MetaData]{
  object uuid       extends UUIDColumn(this) with Index[UUID]
  object name       extends StringColumn(this) with Index[String]
  object category   extends IntColumn(this) with Index[Int]
  object ocr        extends StringColumn(this)
  object ocr_result extends StringColumn(this)
  object date       extends DateTimeColumn(this) with ClusteringOrder[DateTime] with Descending
  object training_v extends OptionalUUIDColumn(this)
  object owner      extends UUIDColumn(this) with PartitionKey[UUID]

  def fromRow(row: Row): MetaData ={
    MetaData(
      uuid       = uuid(row),
      name       = name(row),
      category   = category(row),
      ocr        = ocr(row),
      ocr_result = ocr_result(row),
      date       = date(row),
      training_v = training_v(row),
      owner      = owner(row)
    )
  }
}

abstract class ConcreteMetaDatas extends MetaDatas with RootConnector{
  def getUuidFileName(Owner:UUID): Future[List[(UUID, String)]] = {
    select(_.uuid, _.name).where(_.owner eqs Owner).fetch()
  }

  def getFileData(Owner:UUID): Future[List[(UUID, String, String, DateTime)]] = {
    select(_.uuid, _.name, _.ocr_result, _.date).where(_.owner eqs Owner).fetch()
  }

  def getMetaDataFile(Owner:UUID, LeoFileName:UUID):Future[Option[MetaData]]={
    select.where(_.owner eqs Owner).and(_.uuid eqs LeoFileName).one
  }

  def getDataTable(Category:Int, Owner:UUID):Future[List[(UUID, String, String, DateTime)]] = {       //카테고리별로 출력해야함
    select(_.uuid, _.name, _.ocr_result, _.date).where(_.owner eqs Owner).and(_.category eqs Category).fetch()
  }

  def lastMetadate(Owner:UUID):Future[Option[(UUID, String)]] = {
    select(_.uuid, _.ocr_result).where(_.owner eqs Owner).one()
  }

  def checkName(LeoFileName:UUID, Owner:UUID):Future[Option[Long]] = {
    select.count.where(_.owner eqs Owner).and(_.uuid eqs LeoFileName).one()
  }

  def CountMetaDatas(Category:Int, Owner:UUID):Future[Option[Long]] ={                                  //카테고리별 튜플 수 출력
    select.count.where(_.owner eqs Owner).and(_.category eqs Category).allowFiltering().one()
  }

  def ModiyMetaData(Owner:UUID, Date:DateTime, Category:Int, CassFileName:String, Ocr:String, Ocr_result:String, LeoFileName:UUID):Future[ResultSet] ={
    insert.value(_.owner, Owner).value(_.date, Date).value(_.category, Category).value(_.name, CassFileName).value(_.ocr, Ocr).value(_.ocr_result, Ocr_result)
      .value(_.uuid, LeoFileName).future()
  }

  def setMetaData(LeoFileName:UUID, CassFileName:String, category:Int, Ocr:String, Date:DateTime, Owner:UUID): Future[ResultSet] ={
    insert.value(_.uuid, LeoFileName).value(_.name, CassFileName).value(_.category, category).value(_.ocr, Ocr).value(_.ocr_result, Ocr)
      .value(_.date, Date).value(_.owner, Owner).future()
  }

  def deleteMetaData(Owner:UUID, Date:DateTime):Future[ResultSet]={
    delete.where(_.owner eqs Owner).and(_.date eqs Date).future()
  }

  def deleteOwnerMetaDatas(Owner:UUID):Future[ResultSet]={
    delete.where(_.owner eqs Owner).future()
  }

  def creat(): Future[ResultSet] = {
    create
      .ifNotExists
      .future()
  }

}

object MetaData {

  implicit val MetaDataReads = Json.reads[MetaData] //create json fromat

  implicit val MetaDataWrites = Json.writes[MetaData]

  implicit val MetaDataFormat = Json.format[MetaData]
}


case class UuidFileName(uuid:UUID, name:String)
object UuidFileName{
  implicit val UuidFileNameFormat = Json.format[UuidFileName]
}

case class UuidOcr(uuid:UUID, ocr:String)
object UuidOcr{
  implicit val UuidOcrFormat = Json.format[UuidOcr]
}

case class FileData(uuid:UUID, name:String, date:DateTime)
object FileData{
  implicit val FileDataFormat = Json.format[FileData]
}

case class Search_fileresponData(email:String, data:MetaData)
object Search_fileresponData{
  implicit val responeFormat = Json.writes[Search_fileresponData]
}

case class UuidFileNameOcrDate(uuid:UUID, name:String, ocr:String, date:DateTime)
object UuidFileNameOcrDate{
  implicit val fileNameOcrDateFormat = Json.format[UuidFileNameOcrDate]
}

case class OCR_DataTableResponData(draw:Int=1, recordsTotal:Int=1, recordsFiltered:Int=1, data:List[UuidFileNameOcrDate]=Nil)
object OCR_DataTableResponData{
  implicit val dataTableResponDataFormat = Json.format[OCR_DataTableResponData]
}
