package models
import com.amazonaws._
import com.amazonaws.auth._
import com.amazonaws.services.s3._
import com.amazonaws.services.s3.model._
import java.io._
import java.io.File

import com.amazonaws.services.s3.transfer.{Download, MultipleFileDownload, MultipleFileUpload}


/**
  * Created by yunwansu on 16. 10. 14.
  */
object Amazon {
  val config: ClientConfiguration = new ClientConfiguration();
  config.setProxyHost("121.134.178.250"); // LeoFS Gateway's Host
  config.setProxyPort(8080); // LeoFS Gateway's Port
  config.withSignerOverride("S3SignerType");
  config.withProtocol(Protocol.HTTP);
  //config.setSignerOverride("S3signerType")

  val accessKeyId: String = "7a4a23a0af0d5347c4ee";
  val secretAccessKey: String = "9880fea17adf4b64007606ec052353ffaf5021cb";

  val credentials: AWSCredentials = new BasicAWSCredentials(accessKeyId, secretAccessKey);


  val s3: AmazonS3 = new AmazonS3Client(credentials, config);
  //s3.setEndpoint("wonju05")
  //val acl: AccessControlList = new AccessControlList();
  //        acl.grantPermission(new CanonicalGrantee("7a4a23a0af0d5347c4ee"), Permission.ReadAcp);
  //acl.grantPermission(GroupGrantee.AllUsers, Permission.FullControl);


  val bucketName: String = "test-bucket-5959";
  val key: String = "login.html";

  def create():Int = {
    // Create a bucket
    //print(bucketName)
    try {
      s3.createBucket(bucketName);
    }catch{
      case ase:AmazonServiceException => {
        System.out.println("Caught an AmazonServiceException.");
        System.out.println("Error Message:    " + ase.getMessage());
        System.out.println("HTTP Status Code: " + ase.getStatusCode());
        System.out.println("AWS Error Code:   " + ase.getErrorCode());
        System.out.println("Error Type:       " + ase.getErrorType());
        System.out.println("Request ID:       " + ase.getRequestId());
        return -1
      }
      case ace:AmazonClientException => {
        System.out.println("Caught an AmazonClientException.");
        System.out.println("Error Message: " + ace.getMessage());
        return -2
      }
    }
    return 1;
  }

  def put(key: String, path: String, directory:String):Int = {
    // PUT an object into the LeoFS
    try {
      s3.putObject(new PutObjectRequest(bucketName, directory+"/"+key, new File(path)));
    }catch{
      case ase:AmazonServiceException => {
        System.out.println("Caught an AmazonServiceException.");
        System.out.println("Error Message:    " + ase.getMessage());
        System.out.println("HTTP Status Code: " + ase.getStatusCode());
        System.out.println("AWS Error Code:   " + ase.getErrorCode());
        System.out.println("Error Type:       " + ase.getErrorType());
        System.out.println("Request ID:       " + ase.getRequestId());
        return -1
      }
      case ace:AmazonClientException => {
        System.out.println("Caught an AmazonClientException.");
        System.out.println("Error Message: " + ace.getMessage());
        return -2
      }
    }
    return 1;
  }

  def get(key: String, directory:String):S3Object = {
    val obj:S3Object = s3.getObject(new GetObjectRequest(bucketName, directory+"/"+key));
    //dumpInputStream(obj.getObjectContent());
    //obj
    obj
  }

  def copy(src_key:String, dest_key:String):Int = {
    try {
      s3.copyObject(bucketName, src_key, bucketName, dest_key)
    }catch{
      case ase:AmazonServiceException => {
        System.out.println("Caught an AmazonServiceException.");
        System.out.println("Error Message:    " + ase.getMessage());
        System.out.println("HTTP Status Code: " + ase.getStatusCode());
        System.out.println("AWS Error Code:   " + ase.getErrorCode());
        System.out.println("Error Type:       " + ase.getErrorType());
        System.out.println("Request ID:       " + ase.getRequestId());
        return -1
      }
      case ace:AmazonClientException => {
        System.out.println("Caught an AmazonClientException.");
        System.out.println("Error Message: " + ace.getMessage());
        return -2
      }
    }
    return 1;
  }
  def delete(key: String, directory:String):Int = {
    try {
      s3.deleteObject(bucketName, directory+"/"+key)
    }catch{
      case ase:AmazonServiceException => {
        System.out.println("Caught an AmazonServiceException.");
        System.out.println("Error Message:    " + ase.getMessage());
        System.out.println("HTTP Status Code: " + ase.getStatusCode());
        System.out.println("AWS Error Code:   " + ase.getErrorCode());
        System.out.println("Error Type:       " + ase.getErrorType());
        System.out.println("Request ID:       " + ase.getRequestId());
        return -1
      }
      case ace:AmazonClientException => {
        System.out.println("Caught an AmazonClientException.");
        System.out.println("Error Message: " + ace.getMessage());
        return -2
      }
    }
    return 1;
  }

  def getsize(key:String):Long ={
    var size:Long = 0
    var objectListing: ObjectListing = s3.listObjects(new ListObjectsRequest().withBucketName(bucketName));

    for (i <- 0 to objectListing.getObjectSummaries().size() - 1) {
      if(objectListing.getObjectSummaries().get(i).getKey.equals(key)){
        size = objectListing.getObjectSummaries().get(i).getSize()

        size
      }
    }
    size
  }

  def dumpInputStream(input: InputStream) {
    val reader: BufferedReader = new BufferedReader(new InputStreamReader(input));
    while (true) {
      val line: String = reader.readLine();
      if (line != null) {
  //      print(line);
      } else {
        return;
      }
    }
  }

  def list(directory:String): String = {
    var str:String = ""
    var objectListing: ObjectListing = s3.listObjects(new ListObjectsRequest().withBucketName(bucketName+"/"+directory));

    for (i <- 0 to objectListing.getObjectSummaries().size() - 1) {
      str += objectListing.getObjectSummaries().get(i).getKey+"\n"// + ", " + objectListing.getObjectSummaries().get(i).getLastModified+"\n"
      println("" + objectListing.getObjectSummaries().get(i).getKey + ", " + objectListing.getObjectSummaries().get(i).getLastModified)
    }
    str
  }
}

