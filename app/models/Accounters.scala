package models

import java.util.UUID

import com.websudos.phantom.builder.query.SelectQuery.Default
import com.websudos.phantom.dsl._
import play.api.libs.json.Json

import scala.concurrent.Future

/**
  * Created by root on 16. 9. 29.
  */
case class Account(
                  email:String,
                  name:String,
                  passwd:String,
                  uuid:UUID
                  )

case class display_account(
                          email:String,
                          name:String,
                          passwd:String
                          )

abstract class Accounters extends CassandraTable[ConcreteAccounters, Account]{
    object email  extends StringColumn(this) with PartitionKey[String]
    object name   extends StringColumn(this)
    object passwd extends StringColumn(this) with Index[String]
    object uuid   extends UUIDColumn(this) with PrimaryKey[UUID]
  def fromRow(row:Row):Account = {
     Account(
       email = email(row),
       name = name(row),
       passwd = passwd(row),
       uuid   = uuid(row)
     )
  }
}

abstract class ConcreteAccounters extends Accounters with RootConnector{
  def getCountAccount(Email: String, Passwd: String): Future[Option[Long]] = {
    select.count.where(_.email eqs Email).and(_.passwd eqs Passwd).one
  }
  def getCountAccount(Email: String): Future[Option[Long]] = {
    select.count.where(_.email eqs Email).one
  }

  def getAccount(Email:String, Passwd:String):Future[Option[Account]] = {
    select.where(_.email eqs Email).and(_.passwd eqs Passwd).one
  }

  def setAccount(Email: String, Name: String, Passwd: String): Future[ResultSet] ={
    insert.value(_.email, Email).value(_.name, Name).value(_.passwd, Passwd).value(_.uuid, UUID.randomUUID()).future()
  }


  /*def getByPasswd(Passwd: String): Future[Option[Account]] = {
    val result = select.where(_.passwd eqs Passwd).one()
    result
  }*/
  def getName(Email:String) :Future[Option[String]]={
    select(_.name).where(_.email eqs Email).one
  }

  def deleteAccount(Email:String):Future[ResultSet]={
    delete.where(_.email eqs Email).future()
  }

  def creat(): Future[ResultSet] = {
    create
      .ifNotExists
      .future()
  }

  def JsonTest(Email:String, Passwd:String):Future[List[Account]]={   //json test function
    select.where(_.email eqs Email).fetch()//where(_.email eqs Email).fetch()
  }

  def JsonTestAll():Future[List[Account]]={   //json test function
    select.fetch()
  }

}


object Account {
  implicit val json = Json.format[Account]   //create json fromat
}
/*object Account {
  implicit val json = Json.format[display_account]
}*/
case class DataTables_Account_ResponData(draw:Int=1, recordsTotal:Int=1, recordsFiltered:Int=1, data:List[Account])
object DataTables_Account_ResponData{
  implicit val responeFormat = Json.format[DataTables_Account_ResponData]
}