package models

import java.io.File
import akka.actor._
import net.sourceforge.tess4j.{ITesseract, Tesseract, Tesseract1, TesseractException}

/**
  * Created by root on 16. 10. 19.
  */
object Tesseract {
  def props = Props[Tesseract]

  case class OCR(filePath: String)

}

class Tesseract extends Actor{
  import Tesseract._
  def receive ={
    case OCR(filePath:String) =>{
      //val instance: ITesseract = new Tesseract(); // JNA Interface Mapping
      val instance:ITesseract = new Tesseract1();
      instance.setDatapath("/usr/share/tesseract-ocr/tessdata")
      instance.setLanguage("kor")
      val imageFile: File = new File(filePath);
      try {
        val result: String = instance.doOCR(imageFile);
         System.out.println(result);
        sender() ! result
      } catch {
        case e: TesseractException => {
          //=> println(e.getMessage());
          sender() ! ""
        }
      }
    }
  }
}