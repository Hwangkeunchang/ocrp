package models



import com.websudos.phantom.connectors.{ContactPoint, KeySpaceDef}
import com.websudos.phantom.dsl._


object Defaults {
  val Connector = ContactPoint("121.134.178.246", 9042).withClusterBuilder(_.withCredentials("wonju","dnjswn1@#")).keySpace("websudos")
}

class AppDatabase(val keyspace: KeySpaceDef) extends Database(keyspace) {

  object users extends ConcreteUsers with keyspace.Connector //test table

  object account extends ConcreteAccounters with keyspace.Connector

  object metadata extends ConcreteMetaDatas with keyspace.Connector

  object board extends ConcreteBoards with keyspace.Connector
}


object AppDatabase extends AppDatabase(Defaults.Connector)