package controllers

import java.util.UUID
import javax.inject.{Inject, Singleton}

import akka.actor._
import akka.pattern.ask
import akka.stream.scaladsl.{Source, StreamConverters}
import akka.util.{ByteString, Timeout}
import com.amazonaws.{AmazonClientException, AmazonServiceException}
import com.amazonaws.services.s3.model.S3Object
import models.Tesseract.OCR
import models._
import org.joda.time.DateTime
import play.api.libs.json.{JsValue, Json}
import play.api.mvc.{Action, Controller}

import scala.concurrent.{Await, Future}
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.Duration
import scala.concurrent.duration._

/**
  * Created by root on 16. 10. 27.
  */
@Singleton
class OCR_TableController @Inject()(system: ActorSystem) extends Controller{
  def create_bucket() = Action.async{implicit request =>
    Future {
      //Amazon.create();
      val result = Amazon.create()
      Ok(result.toString);
    }
  }

  implicit val timeout: Timeout = 20.seconds
  def image_upload() = Action.async(parse.multipartFormData) { implicit request =>
    Future {
      //val path: String = request.body.asFormUrlEncoded.get.get("FileName").headOption.get.head
      //val key: String = request.body.asFormUrlEncoded.get.get("Headls").headOption.get.head
      val name = request.body.dataParts.get("name").get.head
      val category = request.body.dataParts.get("category").get.head.toInt
      val file = request.body.file("file").get
      val file_name = file.filename
      val uuid = request.session.get("uuid").get
      val fileUuid = UUID.randomUUID().toString
      import java.io.File
      file.ref.moveTo(new File(s"/tmp/$file_name"), true) // replace -> true
      val result = Amazon.put(fileUuid, s"/tmp/$file_name", uuid)
      if(result == 1) {
        val ocr_str = system.actorOf(Tesseract.props)
        Await.result((ocr_str ? OCR(s"/tmp/$file_name")).mapTo[String].map { message =>
          Await.result(AppDatabase.metadata.setMetaData(UUID.fromString(fileUuid), name, category, message, new DateTime(), UUID.fromString(uuid)), Duration.Inf)},
          Duration.Inf)
      }
      /*request.body.file("file").map { file =>
      val uuid = request.session.get("uuid").get
      import java.io.File

      val contentType = file.contentType
      file.ref.moveTo(new File(s"/tmp/$name"))                         //create /tmp/filename
      val result: PutObjectResult = Amazon.put(name, s"/tmp/$name")   //image upload using s3 API to leofs_storage


      //grayscale(s"/tmp/$filename")
      print(Tesseract.OCR(s"/tmp/$name")) //Tesseract result -> String


      val ocr_Str = Tesseract.OCR(s"/tmp/$name")
      val calendar:Calendar = Calendar.getInstance();
      AppDatabase.metadata.setMetaData(name,category, ocr_Str, new DateTime(), UUID.fromString(uuid))
      //val re = Await.result(AppDatabase.metadata.getMetaData(UUID.fromString("cda8d3ad-0159-433b-8aff-8ede10174256")), Duration.Inf).headOption.get
      //println(Json.toJson(re))
      Ok("File uploaded")
    }.getOrElse {
      //Redirect(routes.Application.index).flashing("error" -> "Missing file")
    }*/
      Ok(result.toString)
    }
  }

  def image_delete() = Action.async{ implicit  request =>
    Future{
      /* val getJson: JsValue = request.body.asJson.get
       val key:String = (getJson \ "key").as[String]
                                                       */ //POST
      val fileUuid:String = request.getQueryString("fileuuid").get
      val user_uuid:String = request.session.get("uuid").get

        val file_info = Await.result(AppDatabase.metadata.getMetaDataFile(UUID.fromString(user_uuid), UUID.fromString(fileUuid)), Duration.Inf).headOption.get
      try {
        val result = Amazon.delete(file_info.uuid.toString, user_uuid)
        if (result > 0) {
          Await.result(AppDatabase.metadata.deleteMetaData(UUID.fromString(user_uuid), file_info.date), Duration.Inf)
        }
        Ok(result.toString)
      }catch {
        case e:Exception =>{
          println(e.getMessage)
          Ok("0")
        }
      }
    }
  }

  def image_size(key:String) = Action.async{implicit request =>
    Future{
      Ok(""+Amazon.getsize(key))
    }
  }

  def last_image() = Action.async{implicit request =>
  Future{
    val uuid = request.session.get("uuid").get
    try {
      val result = Await.result(AppDatabase.metadata.lastMetadate(UUID.fromString(uuid)), Duration.Inf).headOption.get
      Ok(Json.toJson(UuidOcr(result._1, result._2)))
    }catch {
      case e:Exception =>
        Ok("0")
    }
  }}
  def image_download(key:String) = Action.async{ implicit request =>
    Future{
      //val key:String = request.body.asFormUrlEncoded.get.get("Headls").headOption.get.head
      /*      val obj:S3Object = Amazon.get(key)
            val objectData:InputStream = obj.getObjectContent();
            val output:File = new File("output.jpg")
        val file:OutputStream = new FileOutputStream(output)
         var buf:Array[Byte] = new Array[Byte](1024)
        var len:Int = 0
        while((len = objectData.read(buf))>0){
          file.write(buf, 0, len)
        }
        file.close();
            objectData.close();
      */

      //val out:BufferedWriter = new BufferedWriter(new FileWriter(obj.getKey));
      // Process the objectData stream.
      //out.write()
      val CHUNK_SIZE = 100
      var obj:S3Object = null
      println("key => "+ key)
      val uuid:String = request.session.get("uuid").get
      try {
        obj = Amazon.get(key, uuid)
      }catch {
        case ase:AmazonServiceException => {
          System.out.println("Caught an AmazonServiceException.");
          System.out.println("Error Message:    " + ase.getMessage());
          System.out.println("HTTP Status Code: " + ase.getStatusCode());
          System.out.println("AWS Error Code:   " + ase.getErrorCode());
          System.out.println("Error Type:       " + ase.getErrorType());
          System.out.println("Request ID:       " + ase.getRequestId());
          Ok("-1")
        }
        case ace:AmazonClientException => {
          System.out.println("Caught an AmazonClientException.");
          System.out.println("Error Message: " + ace.getMessage());
          Ok("-2")
        }
      }
      val dataContent: Source[ByteString, _] = StreamConverters.fromInputStream(obj.getObjectContent, CHUNK_SIZE)
      Ok.chunked(dataContent).as(contentType = "image/png")
      //Ok(dataContent)//.as(contentType = "image/png")
    }
  }

  def ocr_table_update() = Action.async{implicit request =>
    Future{
      val getJson: JsValue = request.body.asJson.get
      val uuid:UUID        = (getJson \ "uuid")     .as[UUID]
      val name:String = (getJson \ "name").as[String]
      val ocr :String      = (getJson \ "ocr" )     .as[String]
      val category:Int     = (getJson \ "category" ).as[String].toInt
      val user_uuid:String = request.session.get("uuid").get
      try {
        val file_info = Await.result(AppDatabase.metadata.getMetaDataFile(UUID.fromString(user_uuid), uuid), Duration.Inf).headOption.get
        Await.result(AppDatabase.metadata.deleteMetaData(UUID.fromString(user_uuid), file_info.date), Duration.Inf)
        Await.result(AppDatabase.metadata.ModiyMetaData(UUID.fromString(user_uuid), new DateTime(), category, name, file_info.ocr, ocr, file_info.uuid), Duration.Inf)
        Ok("1")
      }catch {
        case e:Exception =>{
          Ok("0")
        }
      }
    }
  }



  def list_object() = Action.async{ implicit  request =>
    Future{
      val uuid = request.session.get("uuid").get
      //grayscale("./tmp/test_sample.png")
      //println(Tesseract.OCR(grayscale("/tmp/gray.png")))
      try {
        Ok(Amazon.list(uuid))
      }catch{
        case ase:AmazonServiceException => {
          System.out.println("Caught an AmazonServiceException.");
          System.out.println("Error Message:    " + ase.getMessage());
          System.out.println("HTTP Status Code: " + ase.getStatusCode());
          System.out.println("AWS Error Code:   " + ase.getErrorCode());
          System.out.println("Error Type:       " + ase.getErrorType());
          System.out.println("Request ID:       " + ase.getRequestId());
          Ok("-1")
        }
        case ace:AmazonClientException => {
          System.out.println("Caught an AmazonClientException.");
          System.out.println("Error Message: " + ace.getMessage());
          Ok("-2")
        }
      }
    }
  }
}
