package controllers

import java.awt.image.{BufferedImage, WritableRaster}
import java.io._
import java.security.MessageDigest
import java.util.{Base64, Calendar, UUID}
import javax.imageio.ImageIO
import javax.inject._

import akka.stream.scaladsl.{Source, StreamConverters}
import akka.util.ByteString
import com.amazonaws.services.s3.model.{PutObjectResult, S3Object}
import models._
import org.joda.time.DateTime
import play.api._
import play.api.libs.Crypto
import play.api.libs.json.{JsValue, Json}
import play.api.mvc._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{Await, Future}
import scala.concurrent.duration.Duration

/**
 * This controller creates an `Action` to handle HTTP requests to the
 * application's home page.
 */
@Singleton
class HomeController @Inject() extends Controller {

  /**
   * Create an Action to render an HTML page with a welcome message.
   * The configuration in the `routes` file means that this method
   * will be called when the application receives a `GET` request with
   * a path of `/`.
   */
  def index = Action {
    Ok(views.html.index("Your new application is ready."))
  }

  val user = new User("32", "2", "3", "4", "5", "3")
  def store() = Action.async{
    Future {
      AppDatabase.users.store(user)
      Ok("")
    }
  }

  def create_test() = Action.async{
    Future{
      AppDatabase.account.creat()
      AppDatabase.metadata.creat()
      Ok("")
    }
  }


  def search_account() = Action.async { implicit request =>
    Future {
      val getJson: JsValue = request.body.asJson.get
      val email:String = (getJson \ "email").as[String]
      val passwd:String = (getJson \ "passwd").as[String]

        print("encryto -> " + digest(passwd) + "\n") //SHA-256 암호화 getbyEmail에 passwd대신 digest(passwd)를 넣어주면됨
        try {
          val result = Await.result(AppDatabase.account.getCountAccount(email, digest(passwd)), Duration.Inf).headOption.get
          val session = Await.result(AppDatabase.account.getAccount(email, digest(passwd)), Duration.Inf).headOption.get

          //val res = Await.result(AppDatabase.account.JsonTest(email, digest(passwd)), Duration.Inf) //return Account object
          //res.foreach(println)
          //print(Json.toJson(res))   //account객체를 json 포맷으로 변환하여 출력
          //Ok(Json.toJson(res))    //return json object
          if (result >= 1) {
            val Session: List[(String, String)] = List(("email", session.email), ("name", session.name), ("uuid", session.uuid.toString))
            Ok(session.name).withSession(Session: _*) //("uuid" -> session.uuid))// "uuid" -> session.uuid)   //  *original code*
          } else {
            Ok("0")
          }
        } catch {
          case e: Exception => {
            Ok("0")
          }
        }
      /*val email = request.body.asFormUrlEncoded.get.get("email").headOption.get.head
      val passwd = request.body.asFormUrlEncoded.get.get("passwd").headOption.get.head
      val result = Await.result(AppDatabase.account.getByEmail(email, passwd),Duration.Inf).get
      if(result >= 1){
        print(email)
        Ok("1")
      }
      else{
        Ok("0")
      }
      //Ok(result+""+email.headOption.get.head + ", " + passwd.headOption.get.head)*/
    }
  }

  def logout = Action.async{implicit  request =>
    Future{
      Redirect("/assets/index.html").withNewSession
      //Ok("").withNewSession  //discard session
    }
  }
  def insert_email() = Action.async{ implicit  request =>
    Future{
      val getJson: JsValue = request.body.asJson.get
      val email :String = (getJson \ "email").as[String]
      try {
        val check = Await.result(AppDatabase.account.getCountAccount(email), Duration.Inf).headOption.get
        printf("이메일" + email)
        print("중복값" + check)
        if (check > 0) {
          print("있는 아이디 입니다.")
          Ok("0")
        } else {
          print("없는 아이디 입니다.")
          Ok("1")
        }
      }catch {
        case e:Exception => Ok("-1")
      }
//      val result = Await.result(AppDatabase.account.setAccount(email, name, digest(password)), Duration.Inf)
//      print(result)
    }
  }
  def insert_account() = Action.async{ implicit  request =>
    Future{
      val getJson: JsValue = request.body.asJson.get
      val email :String = (getJson \ "email").as[String]
      val name  :String = (getJson \ "name").as[String]
      val password:String = (getJson \ "password").as[String]
      print("이메일" + email + "이름" +name + "비밀번호" + password)
      val result = Await.result(AppDatabase.account.setAccount(email, name, digest(password)), Duration.Inf)
      print(result)
     Ok("1")
    }
  }

  def check_account() = Action.async{ implicit request =>
    Future{
      try {
        val getJson:JsValue = request.body.asJson.get
        val passwd :String = (getJson \ "passwd").as[String]
        val email = request.session.get("email").get
        val result = Await.result(AppDatabase.account.getCountAccount(email, digest(passwd)), Duration.Inf).get
        println(result)
        if (result == 1) {
          Ok("1")
        } else {
          Ok("0")
        }
      }catch {
        case e:Exception => Ok("0")
      }
    }
  }

  def delete_account() = Action.async{ implicit  request =>
    Future{
      try {
        val uuid = request.session.get("uuid").get
        val email = request.session.get("email").get
        Await.result(AppDatabase.metadata.deleteOwnerMetaDatas(UUID.fromString(uuid)), Duration.Inf)
        Await.result(AppDatabase.account.deleteAccount(email), Duration.Inf)
        Ok("1")
      }catch{
        case e:Exception => Ok("0")
      }
    }
  }

  def count_all_category() = Action.async{ implicit  request =>
    Future{
      val uuid = request.session.get("uuid").get
      try {
        val eceipt = Await.result(AppDatabase.metadata.CountMetaDatas(0, UUID.fromString(uuid)), Duration.Inf).headOption.get
        val business_card = Await.result(AppDatabase.metadata.CountMetaDatas(1, UUID.fromString(uuid)), Duration.Inf).headOption.get
        val etc = Await.result(AppDatabase.metadata.CountMetaDatas(2, UUID.fromString(uuid)), Duration.Inf).headOption.get
        Ok(eceipt + "/" + business_card + "/" + etc)
      }catch {
        case e:Exception => Ok("0")
      }
    }
  }
  /*-----------------------------------------------------------------*/
  //SHA-256
  def EncodeHex(digest:Array[Byte]):String ={
    val hexString:StringBuffer = new StringBuffer()
    for(i <- 0 to digest.length-1){
      //val hex:String = Integer.toHexString(0xff & encryto_passwd(i))
      //if(encryto_passwd.length == 1) hexString.append('0')
      //hexString.append(hex)
      hexString.append(Integer.toString((digest(i)&0xff) + 0x100, 16).substring(1))
    }
    hexString.toString
  }

  def digest(input:String):String={
    val digest:MessageDigest = MessageDigest.getInstance("SHA-256")
    val encryto_passwd:Array[Byte] = digest.digest(input.getBytes("UTF-8"))
    EncodeHex(encryto_passwd)
  }
  /*-----------------------------------------------------------------*/


  /*-----------------------------------------------------------------*/
  //Data Table
  def datatable() = Action.async { implicit request =>
    Future {
      val uuid = request.session.get("uuid").get
      val category = request.getQueryString("q").get.toInt -1
      //val Draw = request.getQueryString("draw").get.toInt
      //val start = request.getQueryString("start").get.toInt
      //val length = request.getQueryString("length").get.toIn
      try {
        val list = Await.result(AppDatabase.metadata.getDataTable(category, UUID.fromString(uuid)), Duration.Inf) //return Account object
        val result = list.map { case (uuid, name, ocr, date) => UuidFileNameOcrDate(uuid, name, ocr, date) }
        //println("-------------list\n"+category+", "+Draw+", "+start+", "+length+"\n------------")
        print("json->" + Json.toJson(result) + "\n")
        //print("json2->"+ Json.toJson(OCR_DataTableResponData(Draw, cnt, cnt, result)))
        Ok(Json.toJson(result))
      }catch {
        case e:Exception => Ok("")
      }
    }
  }

  def training_datatable() = Action.async{ implicit request =>
    Future{
      val uuid = request.session.get("uuid").get
      try {
        val list = Await.result(AppDatabase.metadata.getFileData(UUID.fromString(uuid)), Duration.Inf)
        val result = list.map { case (uuid, name, ocr, date) => UuidFileNameOcrDate(uuid, name, ocr, date) }
        Ok(Json.toJson(result))
      }catch {
        case e:Exception => Ok("")
      }
    }
  }
  /*----------------------------------------------------------------*/

  def list_object() = Action.async { implicit  request =>
    Future{
      val uuid = request.session.get("uuid").get
//      val list = Await.result(AppDatabase.metadata.getUuidFileName(UUID.fromString(uuid)), Duration.Inf) //return Account object
//      val result = list.map{case (uuid, string) => UuidFileName(uuid, string)}
      val list = Await.result(AppDatabase.metadata.getFileData(UUID.fromString(uuid)), Duration.Inf) //return Account object 11/10 수정
      val result = list.map{case (uuid, name, ocr, date) => UuidFileNameOcrDate(uuid, name, ocr, date)}// 11/10 수정
      println(Json.toJson(result))
      Ok(Json.toJson(result))
    }
  }

  def search_fileMetaData(fileUuid:String) = Action.async{ implicit  request =>
    Future{
      val user_uuid = request.session.get("uuid").get
      try {
        val res = Await.result(AppDatabase.metadata.getMetaDataFile(UUID.fromString(user_uuid), UUID.fromString(fileUuid)), Duration.Inf).get
        if (res.training_v == null) {
          println("null")
        }
        println("json->" + Json.toJson(Search_fileresponData(request.session.get("email").get, res)))
        Ok(Json.toJson(Search_fileresponData(request.session.get("email").get, res)))
      }catch {
        case e:Exception => Ok("0")
      }
    }
  }


  def grayscale(imagefile:String):String ={
      val imginfo:Array[String] = imagefile.split('.')
      val imgName:String = imginfo(0)+"_scale."+imginfo(1)
      val img = ImageIO.read(new File(imagefile))
      val mytype = img.getType
      val raster:WritableRaster = img.getRaster
      val (w,h) = (img.getWidth, img.getHeight)
      (0 until w).foreach { x=>
        (0 until h).foreach { y=>
          val arr = Array.fill[Double](3)(0.0)
          val bgr = raster.getPixel(x,y, arr)
          val avg = (bgr(0) + bgr(1) + bgr(2))/3.0
          raster.setPixel(x,y,Array(avg,avg,avg))
        }
      }
      val copy = new BufferedImage(w,h,mytype)
      copy.setData(raster)
      ImageIO.write(copy, imginfo(1), new File(imgName))
     //TesseractExample.OCR()
      imgName
  }
}
