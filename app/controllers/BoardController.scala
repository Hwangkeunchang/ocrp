package controllers

import java.util.UUID
import javax.inject.{Inject, Singleton}

import models.{AppDatabase, boardData, noticeData, noticeDetailData}
import org.joda.time.DateTime
import play.api.libs.json.{JsValue, Json}
import play.api.mvc.BodyParsers.parse
import play.api.mvc.{Action, Controller}

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future}
import scala.concurrent.ExecutionContext.Implicits.global

/**
  * Created by Hwang on 2016-11-14.
  */
@Singleton
class BoardController  @Inject() extends Controller {

  def board_list() = Action.async { implicit request =>
    Future {
      val res = Await.result(AppDatabase.board.getBoard(), Duration.Inf)
      val result = res.map {
        case (uuid, no, title, names, date, viewss) =>{
          boardData(uuid, no, title, names, date, viewss)
        }
      }
      println("Data??" + result)
      Ok(Json.toJson(result))
    }
  }
  def board_update() = Action.async{implicit request =>
    Future{
      val getJson: JsValue = request.body.asJson.get
      val title:String = (getJson \ "title").as[String]
      val content :String      = (getJson \ "content").as[String]
      val category:Int     = (getJson \ "category").as[Int]
      val uuid:String     = (getJson \ "uuid").as[String]
      //val user_uuid:String = request.session.get("uuid").get
      var result = Await.result(AppDatabase.board.ModiyBoard(UUID.fromString(uuid), title, content, category), Duration.Inf)
      Ok("1")
    }
  }

  def board_delete() = Action.async{implicit request =>
    Future{
      val uuid:String = request.getQueryString("uuid").get
      Await.result(AppDatabase.board.DeleteBoard(UUID.fromString(uuid)), Duration.Inf)
      Ok("1")
    }
  }

  def board_insert() = Action.async { implicit request =>
    Future {
      val json : JsValue = request.body.asJson.get
      val titles :String = (json \ "titles").as[String]
      val contents :String = (json \ "contents").as[String]
      val categorys = (json \ "categorys").as[Int]

      println("title : " + titles)
      println("content : " + contents)
      println("category : " + categorys)

      val names = Await.result(AppDatabase.account.getName(request.session.get("email").get), Duration.Inf).get.toString
      val uuid = request.session.get("uuid").get
      val count = Await.result(AppDatabase.board.getcount(categorys),Duration.Inf).get.toInt
      println("count : " + count)

      val result = Await.result(AppDatabase.board.setBoard(titles, count+1, contents, UUID.fromString(uuid), names, new DateTime(), categorys), Duration.Inf)

      Ok("1")
    }
  }
  /*
    def insert_board() = Action.async { implicit request =>
      Future {

        val names = Await.result(AppDatabase.account.getName(request.session.get("email").get), Duration.Inf).get.toString
        val uuid = request.session.get("uuid").get
        val count = Await.result(AppDatabase.board.getcount(0),Duration.Inf).get.toInt
        println("count : " + count)

        val result = Await.result(AppDatabase.board.setBoard("test notice 2", count+1, "test", UUID.fromString(uuid), names, new DateTime(), 1), Duration.Inf)

        Ok("1")
      }
    }
  */
  def notice_list() = Action.async { implicit request =>
    Future {
      val res = Await.result(AppDatabase.board.getnotice(), Duration.Inf)
      val result = res.map { case (uuid, name, category, title, date) => noticeData(uuid, name, category, title, date)}
      println("Data??" + result)
      Ok(Json.toJson(result))
    }
  }

  def create_board() = Action.async {
    Future {
      AppDatabase.board.creat()
      Ok("")
    }
  }
  def search_board(uuid:String) = Action.async{ implicit  request =>
    Future {
      val res = Await.result(AppDatabase.board.search_board(UUID.fromString(uuid)), Duration.Inf).get

      println(Json.toJson(noticeDetailData(res._1, res._2, res._3, res._4, res._5, res._6)))
      Ok(Json.toJson(noticeDetailData(res._1, res._2, res._3, res._4, res._5, res._6)))
    }
  }
}
